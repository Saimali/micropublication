/* eslint-disable sort-keys */

const {
  chatThreadMessages,
  sendChatMessage,
} = require('./chatMessage/chatMessage.resolver')
const { manuscriptChatThreads } = require('./chatThread/chatThread.resolver')
const {
  createManuscript,
  deleteManuscript,
  handleInvitation,
  manuscript,
  manuscriptMetadataUpdate,
  manuscripts,
  setDataType,
} = require('./manuscript/manuscript.resolver')
const {
  manuscriptVersions,
  saveSubmissionForm,
  setSOApproval,
  submitDecision,
  submitManuscript,
} = require('./manuscriptVersion/manuscriptVersion.resolver')
const {
  manuscriptVersionReviews,
  reinviteReviewer,
  // requestReviewerAttention,
  saveReview,
  submitReview,
  reviewer,
  reviews,
} = require('./review/review.resolver')
const {
  addExternalReviewer,
  cleanUpGlobalTeamMembership,
  globalTeams,
  inviteReviewer,
  members,
  reviewerStatus,
  teams,
  teamsForObject,
  teamMemberUser,
  updateAssignedEditor,
  updateAssignedScienceOfficer,
  updateReviewerPool,
} = require('./team/team.resolver')
const {
  chatMessageUser,
  currentUser,
  login,
  resendVerificationEmail,
  resendVerificationEmailFromLogin,
  resetPassword,
  sendPasswordResetEmail,
  signUp,
  updatePassword,
  updatePersonalInformation,
  updateUsername,
  users,
  verifyEmail,
} = require('./user/user.resolver')

const resolvers = {
  Query: {
    currentUser,
    globalTeams,
    manuscript,
    manuscripts,
    reviews,
    teams,
    users,
  },
  Mutation: {
    addExternalReviewer,
    cleanUpGlobalTeamMembership,
    createManuscript,
    deleteManuscript,
    handleInvitation,
    inviteReviewer,
    login,
    manuscriptMetadataUpdate,
    reinviteReviewer,
    // requestReviewerAttention,
    resendVerificationEmail,
    resendVerificationEmailFromLogin,
    resetPassword,
    saveReview,
    saveSubmissionForm,
    sendChatMessage,
    sendPasswordResetEmail,
    setDataType,
    setSOApproval,
    signUp,
    submitDecision,
    submitManuscript,
    submitReview,
    updateAssignedEditor,
    updateAssignedScienceOfficer,
    updateReviewerPool,
    updatePassword,
    updatePersonalInformation,
    updateUsername,
    verifyEmail,
  },
  ChatMessage: {
    user: chatMessageUser,
  },
  ChatThread: {
    messages: chatThreadMessages,
  },
  Manuscript: {
    chatThreads: manuscriptChatThreads,
    reviewerStatus,
    teams: teamsForObject,
    versions: manuscriptVersions,
  },
  ManuscriptVersion: {
    reviews: manuscriptVersionReviews,
    teams: teamsForObject,
  },
  Review: {
    reviewer,
  },
  Team: {
    members,
  },
  TeamMember: {
    user: teamMemberUser,
  },
}

module.exports = resolvers
