const { transaction } = require('objection')
const clone = require('lodash/clone')
const difference = require('lodash/difference')
const pick = require('lodash/pick')

const {
  Identity,
  Manuscript,
  Team,
  TeamMember,
  User,
} = require('@pubsweet/models')
const logger = require('@pubsweet/logger')

const { REVIEWER_STATUSES } = require('../constants')
const { notify } = require('../../services')

// NO AUTH
const addExternalReviewer = async (_, { manuscriptVersionId, input }, ctx) => {
  const { email, givenNames, surname } = input
  let user, teamMember

  try {
    await transaction(User.knex(), async trx => {
      const userIdentity = await Identity.query(trx).findOne({ email })

      if (userIdentity) {
        // TO DO -- Give feedback that user already exists
        user = await User.query(trx).findById(userIdentity.userId)
      } else {
        user = await User.query(trx).insert({
          agreedTc: false,
          givenNames,
          surname,
        })

        await Identity.query(trx).insert({
          email,
          isConfirmed: false,
          isDefault: true,
          userId: user.id,
        })
      }

      const reviewersTeam = await Team.query(trx).findOne({
        objectId: manuscriptVersionId,
        role: 'reviewer',
      })

      if (!reviewersTeam)
        throw new Error('Add external reviewer: Reviewers team does not exist')

      teamMember = await TeamMember.query(trx).findOne({
        teamId: reviewersTeam.id,
        userId: user.id,
      })

      if (teamMember) {
        throw new Error(
          'Add external reviewer: User has already been added to reviewer team',
        )
      } else {
        teamMember = await TeamMember.query(trx).insert({
          status: REVIEWER_STATUSES.added,
          teamId: reviewersTeam.id,
          userId: user.id,
        })
      }
    })

    return teamMember.id
  } catch (e) {
    logger.error('Add external reviewer: Transaction failed! Rolling back...')
    throw new Error(e)
  }
}

const cleanUpMembership = async (globalTeams, type) => {
  const globalTeam = globalTeams.find(t => t.role === type)
  const globalMemberIdsForType = globalTeam.members.map(m => m.userId)

  // eg. type "editors" will give "editor"
  const manuscriptType = type.slice(0, -1)

  const allManuscriptTeamsForType = await Team.query()
    .eager('users')
    .where({
      role: manuscriptType,
    })

  const toUnassign = []

  allManuscriptTeamsForType.forEach(team => {
    team.users.forEach(user => {
      if (!globalMemberIdsForType.includes(user.id)) {
        toUnassign.push({
          teamId: team.id,
          userId: user.id,
        })
      }
    })
  })

  return Promise.all(
    toUnassign.map(condition =>
      TeamMember.query()
        .delete()
        .where(condition),
    ),
  )
}

/* 
  This ensures that if there are any assigned editors or science officers
  assigned to a manuscript that have been removed from the corresponding global
  team, they are unassigned.
*/
const cleanUpGlobalTeamMembership = async (_, __, ctx) => {
  const globalTeams = await Team.query()
    .eager('members')
    .where({ global: true })

  await cleanUpMembership(globalTeams, 'editors')
  await cleanUpMembership(globalTeams, 'scienceOfficers')

  return true
}

// NO AUTH
const inviteReviewer = async (_, { manuscriptVersionId, input }, context) => {
  const { reviewerId } = input
  let result

  const reviewer = await TeamMember.query()
    .leftJoin('teams', 'team_members.team_id', 'teams.id')
    .findOne({
      objectId: manuscriptVersionId,
      role: 'reviewer',
      userId: reviewerId,
    })

  if (!reviewer)
    throw new Error(
      'Invite reviewer: User was never added to the reviewer pool',
    )

  let alreadyInvited = false
  const { invited, accepted, rejected } = REVIEWER_STATUSES
  const alreadyInvitedStatuses = [invited, accepted, rejected]
  if (alreadyInvitedStatuses.includes(reviewer.status)) alreadyInvited = true

  if (!alreadyInvited) {
    try {
      result = await reviewer.$query().patchAndFetch({
        status: REVIEWER_STATUSES.invited,
      })
    } catch (e) {
      throw new Error(e)
    }
  } else {
    result = reviewer
  }

  notify('reviewerInvited', {
    reviewerId,
    userId: context.user,
    versionId: manuscriptVersionId,
  })

  return result
}

const reviewerStatus = async (manuscript, __, ctx) => {
  const { id: manuscriptId } = manuscript
  const { user: userId } = ctx

  const queryStatus = await Manuscript.query()
    .select('team_members.status')
    .leftJoin(
      'manuscript_versions',
      'manuscripts.id',
      'manuscript_versions.manuscript_id',
    )
    .leftJoin('teams', 'manuscript_versions.id', 'teams.object_id')
    .leftJoin('team_members', 'team_members.team_id', 'teams.id')
    .leftJoin('users', 'users.id', 'team_members.user_id')
    .findOne({
      'manuscripts.id': manuscriptId,
      'teams.role': 'reviewer',
      'users.id': userId,
    })
    .orderBy('manuscript_versions.created', 'desc')
    .limit(1)

  return queryStatus.status
}

const teams = async (_, __, ctx) => {
  const allTeams = await Team.query()
  return allTeams
}

const updateAssignedGlobalUser = async (currentUserId, input, type) => {
  /* 
    Find the editor / science officer team for this manuscript
    If there is an assigned editor / so already, remove them
    Assign the given user id
  */

  const { manuscriptId, userId } = input
  let newAssignedMember

  try {
    await transaction(Team.knex(), async trx => {
      const isAuthor = await TeamMember.query(trx)
        .leftJoin('teams', 'teams.id', 'team_members.team_id')
        .findOne({
          objectId: manuscriptId,
          role: 'author',
        })

      if (isAuthor)
        throw new Error(
          'Team: Update assigned global user: Cannot assign author of the manuscript to be an editor or science officer for it',
        )

      const assignmentTeam = await Team.query(trx).findOne({
        name: `${type}-${manuscriptId}`,
      })

      // There can be only one
      const currentAssignedMember = await TeamMember.query(trx).findOne({
        teamId: assignmentTeam.id,
      })

      if (currentAssignedMember) {
        await TeamMember.query(trx).deleteById(currentAssignedMember.id)
      }

      newAssignedMember = await TeamMember.query(trx).insert({
        teamId: assignmentTeam.id,
        userId,
      })

      // TO DO -- currentlyWith should be deprecated
      await Manuscript.query(trx)
        .where({ id: manuscriptId })
        .patch({ currentlyWith: userId })
    })

    if (currentUserId !== userId) {
      notify('currentlyWith', {
        currentlyWithId: userId,
        manuscriptId,
        userId: currentUserId,
      })
    }

    // TO DO -- this will not work if we introduce an option to simply
    // unassign a member (instead of assigning someone else)
    return newAssignedMember.userId
  } catch (e) {
    logger.error('Team: Update assigned global user: Failed! Rolling back...')
    throw new Error(e)
  }
}

// NO AUTH
const updateAssignedEditor = async (_, { input }, ctx) =>
  updateAssignedGlobalUser(ctx.user, input, 'editor')

// NO AUTH
const updateAssignedScienceOfficer = async (_, { input }, ctx) =>
  updateAssignedGlobalUser(ctx.user, input, 'science-officer')

// NO AUTH
const updateReviewerPool = async (_, { manuscriptVersionId, input }, ctx) => {
  const { reviewerIds } = input

  const reviewerTeam = await Team.query()
    .findOne({
      objectId: manuscriptVersionId,
      role: 'reviewer',
    })
    .eager('members.user')

  let membersToUpsert = clone(reviewerTeam.members)

  // remove members that have been removed from the team
  membersToUpsert = membersToUpsert.filter(m => reviewerIds.includes(m.user.id))
  // remove unnecessary data
  membersToUpsert = membersToUpsert.map(m => pick(m, ['id', 'user.id']))
  // add new members to array
  const newMemberIds = difference(
    reviewerIds,
    membersToUpsert.map(i => i.user.id),
  )
  newMemberIds.forEach(id => {
    const status = 'notInvited'
    membersToUpsert.push({ status, user: { id } })
  })

  const knex = Team.knex()
  try {
    await transaction(knex, async trx => {
      await Team.query(trx).upsertGraph(
        {
          id: reviewerTeam.id,
          members: membersToUpsert,
        },
        { relate: true },
      )
    })
  } catch (e) {
    logger.error('Update reviewer pool: Transaction failed! Rolling back...')
    throw e
  }

  return true
}

const teamsForObject = async (object, __, ctx) =>
  Team.query().where({
    objectId: object.id,
  })

const globalTeams = async (_, __, ctx) => {
  let globalTs
  try {
    globalTs = await Team.query()
      .where('global', true)
      .eager('[members.[user]]')
  } catch (e) {
    logger.error(e)
    globalTs = []
  }
  return globalTs
}

const members = async (team, vars, ctx) =>
  TeamMember.query().where({
    teamId: team.id,
  })

const teamMemberUser = async (teamMember, vars, ctx) =>
  User.findById(teamMember.userId)

/* eslint-disable-next-line import/prefer-default-export */
module.exports = {
  addExternalReviewer,
  cleanUpGlobalTeamMembership,
  globalTeams,
  inviteReviewer,
  members,
  reviewerStatus,
  teamMemberUser,
  teams,
  teamsForObject,
  updateAssignedEditor,
  updateAssignedScienceOfficer,
  updateReviewerPool,
}
