const clone = require('lodash/clone')
const config = require('config')
const crypto = require('crypto')
const querystring = require('querystring')
const moment = require('moment')
const nodemailer = require('nodemailer')
const { transaction } = require('objection')

const logger = require('@pubsweet/logger')
const {
  AuthorizationError,
  ConflictError,
  ValidationError,
} = require('@pubsweet/errors')
const { Identity, User } = require('@pubsweet/models')
const authentication = require('pubsweet-server/src/authentication')
const { auth, notify } = require('../../services')

/* eslint-disable import/no-dynamic-require */
const mailerOptions = require(config.get('mailer.path'))

const chatMessageUser = async chatMessage => {
  try {
    const user = await User.findById(chatMessage.userId)
    return user
  } catch (e) {
    logger.error('User: Chat message user: Failed!')
    throw new Error(e)
  }
}

const currentUser = async (_, { input }, ctx) => {
  const userId = ctx.user
  if (!userId) throw new Error('Current User: No user id provided')

  const user = await User.findById(userId)

  const { createClientAuth } = auth
  user.auth = await createClientAuth(userId)
  return user
}

const login = async (_, { input }, ctx) => {
  const { username, password } = input
  let isValid = false
  let user

  try {
    user = await User.query().findOne({ username })
    if (!user) throw new AuthorizationError('Wrong username or password.')
    // throw new Error(`Login: No user with username ${username} exists`)

    isValid = await user.validPassword(password)
    if (!isValid) {
      throw new AuthorizationError('Wrong username or password.')
    }

    const identities = await Identity.query().where({ userId: user.id })
    const isConfirmed = identities.some(id => id.isConfirmed)
    if (!isConfirmed) throw new Error('Login: Identity not confirmed')

    return {
      token: authentication.token.create(user),
    }
  } catch (e) {
    logger.error('Login: Failed!')
    throw new Error(e)
  }
}

const sendPasswordResetEmail = async (_, { username }, ctx) => {
  // // fail early if these configs are missing
  const baseUrl = config.get('pubsweet-server.baseUrl')
  const configSender = config.get('mailer.from')

  const pathToPage = config.has('password-reset.pathToPage')
    ? config.get('password-reset.pathToPage')
    : '/password-reset'
  const tokenLength = config.has('password-reset.token-length')
    ? config.get('password-reset.token-length')
    : 32

  const user = await User.query().findOne({ username })
  const identity = await Identity.query().findOne({
    isDefault: true,
    userId: user.id,
  })

  const resetToken = crypto.randomBytes(tokenLength).toString('hex')

  await user.$query().patch({
    passwordResetTimestamp: new Date(),
    passwordResetToken: resetToken,
  })

  const token = querystring.encode({
    token: resetToken,
    username,
  })

  const passwordResetURL = `${baseUrl}${pathToPage}?${token}`

  logger.info(`Sending password reset email to ${user.email}`)

  const options = mailerOptions
  const transport = nodemailer.createTransport(options.transport)

  const content = `
    <p>
      Follow the link below to reset your password in the microPublication
      platform.
    </p>
    <p><a href="${passwordResetURL}">Reset your password</a></p>
  `

  const mailData = {
    from: configSender,
    html: content,
    subject: 'microPublication | Password reset',
    text: `Reset your password: ${passwordResetURL}`,
    to: identity.email,
  }

  await transport.sendMail(mailData)

  return true
}

const resendVerificationEmail = async (_, { token }) => {
  try {
    const identity = await Identity.query().findOne({
      confirmationToken: token,
    })

    if (!identity)
      throw new Error(
        'Resend Verification Email: Token does not correspond to an identity',
      )

    const confirmationToken = crypto.randomBytes(64).toString('hex')
    const confirmationTokenTimestamp = new Date()

    await identity.$query().patch({
      confirmationToken,
      confirmationTokenTimestamp,
    })

    notify('identityVerification', {
      confirmationToken,
      email: identity.email,
    })

    return true
  } catch (e) {
    logger.error('Resend Verification Email: Something went wrong')
    throw new Error(e)
  }
}

const resendVerificationEmailFromLogin = async (_, { username, password }) => {
  try {
    const user = await User.query().findOne({ username })
    if (!user)
      throw new Error(
        `Resend Verification Email From Login: No user with username ${username} found`,
      )

    const identity = await Identity.query().findOne({
      isDefault: true,
      userId: user.id,
    })

    if (!identity)
      throw new Error(
        `Resend Verification Email From Login: No default identity found for user with id ${user.id}`,
      )

    const confirmationToken = crypto.randomBytes(64).toString('hex')
    const confirmationTokenTimestamp = new Date()

    await identity.$query().patch({
      confirmationToken,
      confirmationTokenTimestamp,
    })

    notify('identityVerification', {
      confirmationToken,
      email: identity.email,
    })

    return true
  } catch (e) {
    logger.error('Resend Verification Email From Login: Something went wrong')
    throw new Error(e)
  }
}

const resetPassword = async (_, { token, password }, ctx) => {
  const user = await User.query().findOne({ passwordResetToken: token })
  if (!user) throw new Error('Reset password: Token not found')

  if (
    moment()
      .subtract(24, 'hours')
      .isAfter(user.passwordResetTimestamp)
  ) {
    throw new ValidationError('Your token has expired')
  }

  await user.$query().patch({
    password,
    passwordResetTimestamp: null,
    passwordResetToken: null,
  })

  return user.id
}

/* eslint-disable-next-line consistent-return */
const signUp = async (_, { input }, ctx) => {
  const userInput = clone(input)
  const { email, givenNames, password, surname, username } = userInput

  const usernameExists = await User.query().findOne({ username })
  if (usernameExists) {
    logger.error('Sign up: Username already exists')
    throw new ConflictError('Username already exists')
  }

  const existingIdentity = await Identity.query().findOne({ email })

  if (existingIdentity) {
    const user = await User.query().findById(existingIdentity.userId)

    if (user.agreedTc) {
      logger.error('Sign up: A user with this email already exists')
      throw new ConflictError('A user with this email already exists')
    }

    // If not agreed to tc, user's been invited but is now signing up
    delete userInput.email

    try {
      let updatedUser, confirmationToken

      await transaction(User.knex(), async trx => {
        updatedUser = await User.query(trx).patchAndFetchById(
          existingIdentity.userId,
          {
            ...userInput,
            agreedTc: true,
          },
        )

        confirmationToken = crypto.randomBytes(64).toString('hex')
        const confirmationTokenTimestamp = new Date()

        await existingIdentity.$query(trx).patch({
          confirmationToken,
          confirmationTokenTimestamp,
        })
      })

      notify('identityVerification', {
        confirmationToken,
        email,
      })

      return updatedUser
    } catch (e) {
      throw new Error(e)
    }
  }

  if (!existingIdentity) {
    try {
      let newUser
      const knex = User.knex()

      await transaction(knex, async trx => {
        newUser = await User.query(trx).insert({
          agreedTc: true,
          givenNames,
          password,
          surname,
          username,
        })

        const confirmationToken = crypto.randomBytes(64).toString('hex')
        const confirmationTokenTimestamp = new Date()

        await Identity.query(trx).insert({
          confirmationToken,
          confirmationTokenTimestamp,
          email,
          isConfirmed: false,
          isDefault: true,
          userId: newUser.id,
        })

        notify('identityVerification', {
          confirmationToken,
          email,
        })
      })

      return User.findById(newUser.id)
    } catch (e) {
      logger.error('Signup: User creation failed! Rolling back...')
      throw new Error(e)
    }
  }
}

// NO AUTH
const updatePassword = async (_, { input }, ctx) => {
  const userId = ctx.user
  const { currentPassword, newPassword } = input

  try {
    const u = await User.updatePassword(userId, currentPassword, newPassword)
    return u.id
  } catch (e) {
    throw new Error(e)
  }
}

// NO AUTH
const updatePersonalInformation = async (_, { input }, ctx) => {
  const userId = ctx.user
  const { givenNames, surname } = input

  return User.query().patchAndFetchById(userId, {
    givenNames,
    surname,
  })
}

// NO AUTH
const updateUsername = async (_, { input }, ctx) => {
  const userId = ctx.user
  const { username } = input

  return User.query().patchAndFetchById(userId, { username })
}

const users = async (_, __, ctx) => {
  const all = await User.query()
  const withIdentity = all.map(u => User.findById(u.id))
  return withIdentity
}

const verifyEmail = async (_, { token }, ctx) => {
  try {
    const identity = await Identity.query().findOne({
      confirmationToken: token,
    })

    if (!identity) throw new Error('Verify email: Invalid token')

    if (identity.isConfirmed)
      throw new Error('Verify email: Identity has already been confirmed')

    if (!identity.confirmationTokenTimestamp) {
      throw new Error(
        'Verify email: Confirmation token does not have a corresponding timestamp',
      )
    }

    if (
      moment()
        // .subtract(5, 'seconds')
        .subtract(24, 'hours')
        .isAfter(identity.confirmationTokenTimestamp)
    ) {
      throw new Error('Verify email: Token expired')
    }

    await identity.$query().patch({
      // confirmationToken: null,
      // confirmationTokenTimestamp: null,
      isConfirmed: true,
    })

    return true
  } catch (e) {
    throw new Error(e)
  }
}

module.exports = {
  chatMessageUser,
  currentUser,
  login,
  resendVerificationEmail,
  resendVerificationEmailFromLogin,
  resetPassword,
  sendPasswordResetEmail,
  signUp,
  updatePassword,
  updatePersonalInformation,
  updateUsername,
  users,
  verifyEmail,
}
