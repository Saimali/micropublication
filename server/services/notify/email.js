const config = require('config')
const cheerio = require('cheerio')
const without = require('lodash/without')
const isEmpty = require('lodash/isEmpty')

const mailer = require('@pubsweet/component-send-email')
const {
  Identity,
  // Manuscript,
  ManuscriptVersion,
  Review,
  Team,
  User,
} = require('@pubsweet/models')

/* Helpers */

const baseUrl = config.get('pubsweet-server.baseUrl')
const dashboardUrl = `${baseUrl}/dashboard`
const dashboardLink = `
  <p>
    <a href="${dashboardUrl}">
      View it on your dashboard
    </a>
  </p>
`

const formatManuscriptTitle = str => {
  const $ = cheerio.load(str)
  /* eslint-disable-next-line no-param-reassign */
  const formatted = $('p').each((i, item) => (item.tagName = 'span'))
  return formatted
}

const getArticleUrl = articleId => `${baseUrl}/article/${articleId}`
const getArticleLink = articleId => `
  <p>
    <a href="${getArticleUrl(articleId)}">
      View it on the microPublication platform.
    </a>
  </p>
`

const getAssignedEditorIds = async manuscriptId => {
  const editorTeam = await Team.query()
    .findOne({
      objectId: manuscriptId,
      role: 'editor',
    })
    .eager('users')

  return editorTeam.users.map(u => u.id)
}

// Try to get assigned editors. If not found, return all editors.
const getAssignedOrAllEditorIds = async manuscriptId => {
  const assignedEditorIds = await getAssignedEditorIds(manuscriptId)

  if (isEmpty(assignedEditorIds)) return getEditorIds()
  return assignedEditorIds
}

const getAssignedScienceOfficerIds = async manuscriptId => {
  const scienceOfficerTeam = await Team.query()
    .findOne({
      objectId: manuscriptId,
      role: 'scienceOfficer',
    })
    .eager('users')

  return scienceOfficerTeam.users.map(u => u.id)
}

const getAuthorIds = async versionId => {
  const authorTeam = await Team.query()
    .findOne({
      objectId: versionId,
      role: 'author',
    })
    .eager('users')

  return authorTeam.users.map(u => u.id)
}

const getAuthorEmails = async versionId => {
  const authorIds = await getAuthorIds(versionId)

  const authors = await Promise.all(
    authorIds.map(async authorId => User.findById(authorId)),
  )

  const identities = await Promise.all(
    authors.map(user =>
      Identity.query().findOne({
        isDefault: true,
        userId: user.id,
      }),
    ),
  )

  const emails = identities.map(identity => identity.email).join(',')
  return emails
}

const getCurrentUser = async context => {
  const { userId } = context
  return User.findById(userId)
}

const getEditorIds = async () => {
  const editorTeam = await Team.query()
    .findOne({
      global: true,
      role: 'editors',
    })
    .eager('users')

  return editorTeam.users.map(u => u.id)
}

const getEditorEmails = async () => {
  const editorIds = await getEditorIds()
  return getEmailsByUserIds(editorIds)
}

const getEmailsByUserIds = async userIds => {
  const users = await Promise.all(userIds.map(async id => User.findById(id)))

  const identities = await Promise.all(
    users.map(async user =>
      Identity.query().findOne({
        isDefault: true,
        userId: user.id,
      }),
    ),
  )

  const emails = identities.map(identity => identity.email).join(',')
  return emails
}

const getFirstVersion = async manuscriptId =>
  ManuscriptVersion.query()
    .leftJoin(
      'manuscripts',
      'manuscripts.id',
      'manuscript_versions.manuscript_id',
    )
    .select('manuscript_versions.*', 'isInitiallySubmitted')
    .where({
      isInitiallySubmitted: true,
      manuscriptId,
    })
    .orderBy('created', 'asc')
    .first()

const getLastSubmittedVersion = async manuscriptId =>
  ManuscriptVersion.query()
    .where({
      manuscriptId,
      submitted: true,
    })
    .orderBy('created', 'desc')
    .first()

const getLastVersion = async manuscriptId =>
  ManuscriptVersion.query()
    .where({ manuscriptId })
    .orderBy('created', 'desc')
    .first()

const getReviewById = async reviewId =>
  Review.query()
    .findById(reviewId)
    .throwIfNotFound()

// const getManuscriptById = async id =>
//   Manuscript.query()
//     .findById(id)
//     .throwIfNotFound()

// const getScienceOfficerIds = async () => {
//   const globalTeams = await Team.findByField('global', true)
//   const scienceOfficerTeam = globalTeams.find(
//     t => t.role === 'scienceOfficers',
//   )
//   return scienceOfficerTeam.members
// }

const getVersionById = async id =>
  ManuscriptVersion.query()
    .findById(id)
    .throwIfNotFound()

const getUserById = async userId => User.findById(userId)

const sendEmail = data => {
  const { content, subject, to } = data

  const emailData = {
    from: config.get('mailer.from'),
    html: `<p>${content}</p>`,
    subject: `${subject}`,
    text: content,
    to,
  }

  mailer.send(emailData)
}

const toRegularText = text =>
  text
    // insert a space before all caps
    .replace(/([A-Z])/g, ' $1')
    // uppercase the first character
    .replace(/^./, str => str.toUpperCase())

/* End Helpers */

/* 
  Sends article acceptance email to author
*/
const articleAccepted = async context => {
  const { version } = context
  const authorEmails = await getAuthorEmails(version.id)

  const content = `
    <p>
      Your article "${formatManuscriptTitle(version.title)}" 
      has been accepted by the editors!
    </p>
    <h4>
      Decision letter:
    </h4>
    <p>
      ${version.decisionLetter}
    </p>
    ${dashboardLink}
  `

  const data = {
    content,
    subject: 'Article accepted',
    to: authorEmails,
  }

  sendEmail(data)
}

/* 
  Sends article rejection email to author
*/
const articleRejected = async context => {
  const { version } = context
  const authorEmails = await getAuthorEmails(version.id)

  const content = `
    <p>
      Your article "${formatManuscriptTitle(version.title)}" 
      has been rejected by the editors.
    </p>
    <h4>
      Decision letter:
    </h4>
    <p>
      ${version.decisionLetter}
    </p>
    ${dashboardLink}
  `

  const data = {
    content,
    subject: 'Article rejected',
    to: authorEmails,
  }

  sendEmail(data)
}

/* 
  Sends request for article revision to author
*/
const articleRevision = async context => {
  const { version } = context
  const authorEmails = await getAuthorEmails(version.id)

  const content = `
    <p>
      The editors have requested revisions for your article 
      "${formatManuscriptTitle(version.title)}".
    </p>
    <h4>
      Decision letter:
    </h4>
    <p>
      ${version.decisionLetter}
    </p>
    ${getArticleLink(version.manuscriptId)}
  `

  const data = {
    content,
    subject: 'Article revision requested',
    to: authorEmails,
  }

  sendEmail(data)
}

/* 
  Sends email when the chat in the editor panel is used.
  Email is sent to all other assigned editors / science officers.
*/
const chat = async context => {
  const { messageContent, manuscriptId, reviewerId, type, userId } = context

  const user = await getUserById(userId)

  /*
    Generally, send the message to the assigned editor(s).
    If none are found, just send it to all global editors.
  */
  let editorIds
  const assignedEditorIds = await getAssignedEditorIds(manuscriptId)
  if (isEmpty(assignedEditorIds)) {
    editorIds = await getEditorIds()
  } else {
    editorIds = assignedEditorIds
  }

  /*
    Get the latest submitted version, so that the authors are the last authors
    that have been known to the editors. Same for the title.
    If there is no submitted version (after initial submission, before full),
    just get the first version of the manuscript (provided it's been submitted at all).
  */
  let version
  const lastSubmittedVersion = await getLastSubmittedVersion(manuscriptId)
  if (!lastSubmittedVersion) {
    version = await getFirstVersion(manuscriptId)
  } else {
    version = lastSubmittedVersion
  }
  if (!version) throw new Error('Email: Chat: No version found!')

  let otherIds
  if (type === 'scienceOfficer') {
    otherIds = await getAssignedScienceOfficerIds(manuscriptId)
  }
  if (type === 'author') {
    otherIds = await getAuthorIds(version.id)
  }
  if (type === 'reviewer' && reviewerId) {
    otherIds = [reviewerId]
  }

  // Send to all assigned editor and scienceOfficer ids, apart form sender
  const assigned = new Set([...editorIds, ...otherIds])
  assigned.delete(userId)

  const users = await Promise.all(
    [...assigned].map(id => User.query().findById(id)),
  )

  const identities = await Promise.all(
    users.map(u =>
      Identity.query().findOne({
        isDefault: true,
        userId: u.id,
      }),
    ),
  )
  const sendTo = identities.map(u => u.email).join(',')

  const content = `
    <p>
      User ${user.username} sent the following message for article
      "${formatManuscriptTitle(version.title)}":
    </p>
    <p>
      ${messageContent}
    </p>
    ${getArticleLink(manuscriptId)}
  `

  const data = {
    content,
    subject: 'New chat message',
    to: sendTo,
  }

  sendEmail(data)
}

/*
  Sends email to user when the "Send to science officer / editor" is clicked
*/
const currentlyWith = async context => {
  const { currentlyWithId, manuscriptId } = context
  const version = await getLastSubmittedVersion(manuscriptId)

  const identity = await Identity.query().findOne({
    isDefault: true,
    userId: currentlyWithId,
  })
  const sendTo = identity.email
  const currentUser = await getCurrentUser(context)

  const content = `
    <p>
      Your attention was requested by user ${currentUser.username} on article
      "${formatManuscriptTitle(version.title)}".
    </p>
    ${getArticleLink(manuscriptId)}
  `

  const data = {
    content,
    subject: 'Attention requested',
    to: sendTo,
  }

  sendEmail(data)
}

/* 
  Sends email to authors that a data type has been selected on their article
*/
const dataTypeSelected = async context => {
  const { manuscript } = context
  const version = await getLastVersion(manuscript.id)
  const authorEmails = await getAuthorEmails(version.id)
  const noDatatype = manuscript.dataType === 'noDatatype'

  const nextStepText = noDatatype
    ? `
        <p>
          Your article is now with the editorial team for review.
        </p>
      `
    : `
        <p>
          You can now complete your full submission! Scroll down to the bottom 
          of the submission and fill out the relevant fields.
        </p>
      `

  const content = `
    <p>
      The microPublication editors assigned your article 
      "${formatManuscriptTitle(version.title)}"
      the "${toRegularText(manuscript.dataType)}" data type.
    </p>
    ${nextStepText}    
    ${getArticleLink(manuscript.id)}
  `

  const data = {
    content,
    subject: 'Data type selected for submission',
    to: authorEmails,
  }

  sendEmail(data)
}

/* 
  Sends email to editors that an article is now fully submitted
*/
const fullSubmission = async context => {
  // const manuscript = await getManuscript(context)
  const { version } = context
  const currentUser = await getCurrentUser(context)

  /*
    There is one case (when 'no datatype' datatype is chosen) where the editor
    triggers the full submission, not the author. In this scenario, send the
    email to all other editors.
  */
  let editorIds = await getAssignedOrAllEditorIds(version.manuscriptId)

  const isCurrentUserEditor = editorIds.includes(currentUser.id)
  if (isCurrentUserEditor) editorIds = without(editorIds, currentUser.id)
  const editorEmails = await getEmailsByUserIds(editorIds)

  const message = isCurrentUserEditor
    ? `
        <p>
          Article "${formatManuscriptTitle(version.title)}" has been fully
          submitted and is now ready for review.
        </p>
      `
    : `
        <p>
          User ${currentUser.username} just finished the full submission for
          article "${formatManuscriptTitle(version.title)}".
        </p>
      `

  const content = `
    ${message}
    ${getArticleLink(version.manuscriptId)}
  `

  const data = {
    content,
    subject: 'Full Submission',
    to: editorEmails,
  }

  sendEmail(data)
}

/*
  Sends email with email verification token to new users
*/
const identityVerification = async context => {
  const { confirmationToken, email } = context
  const link = `${baseUrl}/email-verification/${confirmationToken}`

  const content = `
    <p>Thank you for signing up!</p>
    <p>Click on <a href="${link}">this link</a> to verify your account.</p>
  `

  const data = {
    content,
    subject: 'Account Verification',
    to: email,
  }

  sendEmail(data)
}

/* 
  Sends email to editors that a new article has been submitted
*/
const initialSubmission = async context => {
  const editorEmails = await getEditorEmails()
  const { version } = context
  const currentUser = await getCurrentUser(context)

  const content = `
    <p>There has been a new submission!</p>
    <p>
      User ${
        currentUser.username
      } just submitted article "${formatManuscriptTitle(version.title)}".
    </p>
    ${getArticleLink(version.manuscriptId)}
  `

  const data = {
    content,
    subject: 'New Submission',
    to: editorEmails,
  }

  sendEmail(data)
}

/* 
  Sends an email to the reviewer. The editor triggers this manually to
  draw the reviewer's attention to a specific manuscript (most likely because
  of a new version).
*/
const reinviteReviewer = async context => {
  const { manuscriptId, reviewerId } = context
  // const manuscript = await getManuscriptById(manuscriptId)
  const version = await getLastSubmittedVersion(manuscriptId)

  const userToNotify = await getUserById(reviewerId)
  const userToNotifyIdentity = await Identity.query().findOne({
    isDefault: true,
    userId: userToNotify.id,
  })
  // const currentUser = await getCurrentUser(context)

  const content = `
    <p>
      Please see the revised version for the article "${formatManuscriptTitle(
        version.title,
      )}" and let us know if you are satisfied with the author's revisions.
    </p>
    ${dashboardLink}
  `

  const data = {
    content,
    subject: 'Attention requested',
    to: userToNotifyIdentity.email,
  }

  sendEmail(data)
}

/* 
  Sends email to editors when a reviewer has responded to an invitation
*/
const reviewerInvitationResponse = async context => {
  const { action, versionId } = context

  if (action !== 'accept' && action !== 'reject')
    throw new Error(`
    Reviewer Invitation Response: Invalid action ${action} provided
    `)

  const version = await getVersionById(versionId)
  const editorIds = await getAssignedOrAllEditorIds(version.manuscriptId)
  const editorEmails = await getEmailsByUserIds(editorIds)
  const currentUser = await getUserById(context.userId)

  const invitationText = action === 'accept' ? 'accepted' : 'rejected'

  const content = `
    <p>
      User ${currentUser.username} has ${invitationText} your invitation
      to review article "${formatManuscriptTitle(version.title)}"
    </p>
    ${getArticleLink(version.manuscriptId)}
  `

  const data = {
    content,
    subject: `Reviewer invitation ${invitationText}`,
    to: editorEmails,
  }

  sendEmail(data)
}

/* 
  Send email to reviewer when they are invited to review an article
*/
const reviewerInvited = async context => {
  const { reviewerId, versionId } = context
  const version = await getVersionById(versionId)
  const reviewer = await getUserById(reviewerId)
  const identity = await Identity.query().findOne({
    isDefault: true,
    userId: reviewerId,
  })
  const sendTo = identity.email
  const name = await reviewer.displayName

  const secondPart = reviewer.agreedTc
    ? dashboardLink
    : `
    <p>
      <em>microPublication Biology</em> publishes single experimental findings, with the goal of rapid dissemination to the 
      community through a citable article and seamless incorporation into research databases. You can learn more about the 
      microPublication project at <a href="https://www.micropublication.org">micropublication.org</a>.  
    </p>

    <p>
      As <em>microPublication Biology</em> publishes single experiment findings, review of the manuscript will only 
      require a few minutes of your time. The review criteria is simple:
      <ul>
        <li>
          Are results technically sound and do they adhere to reporting guidelines and community standards?
        </li>
        <li>
          Do the results, and related information (e.g. statistical analysis), support the conclusions drawn?
        </li>
        <li>
          Are the experiments sufficiently explained/referenced so that the findings can be reproduced by other researchers?
        </li>
        <li>
          Is the microPublication presented in a logical progression, in standard English, and with appropriate nomenclature?
        </li>
      </ul>
      You can find more information in the Guidelines for reviewers <a href="https://www.micropublication.org/about/for-reviewers/">here</a>.
    </p>
 
    <p>
      To review the article, click on this link to <a href="${baseUrl}/signup">sign up</a> <strong>with the email address ${identity.email}</strong> to create an account.
    </p>
    `

  const content = `
    <p>
      Dear ${name},
    </p>
    
    <p>
      We invite you to review the article "${formatManuscriptTitle(
        version.title,
      )}" for <em>microPublication Biology</em>.<br />
      You have been chosen as a reviewer with expertise in the relevant research area either by the Editorial group or 
      recommended by a colleague.
    </p>

    ${secondPart}

    <p>
      We will be most appreciative if you could reply within a week, our aim is to get these data to the community in a 
      timely fashion. Should you have any questions, please do not hesitate to contact us at 
      <a href="mailto:contact@micropublication.org">contact@micropublication.org</a>.
    </p>
    
    <p>
      Thank you in advance for your time, and we look forward to hearing from you.
    </p>

    <p>
      microPublication Editorial Team
    </p>
  `

  const data = {
    content,
    subject: 'Invitation to review',
    to: sendTo,
  }

  sendEmail(data)
}

/* 
  Send email to editors when a review is submitted
*/
const reviewSubmitted = async context => {
  const { reviewId, userId } = context
  const review = await getReviewById(reviewId)
  const version = await getVersionById(review.articleVersionId)
  const reviewer = await getUserById(userId)
  const reviewerEmail = await getEmailsByUserIds([userId])
  const reviewerName = await reviewer.displayName

  const editorIds = await getAssignedOrAllEditorIds(version.manuscriptId)
  const editorEmails = await getEmailsByUserIds(editorIds)

  const content = `
    <p>
      User ${reviewer.username} just submitted a review for article 
      "${formatManuscriptTitle(version.title)}"!
    </p>
    ${getArticleLink(version.manuscriptId)}
  `

  const data = {
    content,
    subject: 'Review submitted',
    to: editorEmails,
  }

  sendEmail(data)

  const reviewerContent = `
    <p>
      Dear ${reviewerName},
    </p>
    <p>
      Thank you for reviewing. We appreciate your time and effort.
    </p>
    <p>
      microPublication Editorial Team
    </p>
  `

  const reviewerData = {
    content: reviewerContent,
    subject: 'Review submitted',
    to: reviewerEmail,
  }

  sendEmail(reviewerData)
}

/* 
  Sends email to assigned editors, when the author submits a revision
*/
const revisionSubmitted = async context => {
  const { version } = context

  const editorIds = await getAssignedOrAllEditorIds(version.manuscriptId)
  const editorEmails = await getEmailsByUserIds(editorIds)

  const content = `
    <p>
      A revision for manuscript ${formatManuscriptTitle(version.title)}
      has been submitted.
    </p>
    ${getArticleLink(version.manuscriptId)}
  `

  const data = {
    content,
    subject: 'Revision submitted',
    to: editorEmails,
  }

  sendEmail(data)
}

/* 
  Sends email to editors when the science officer changes the approval status
  of an article
*/
const scienceOfficerApprovalStatusChange = async context => {
  const version = await getVersionById(context.versionId)

  const editorIds = await getAssignedOrAllEditorIds(version.manuscriptId)
  const editorEmails = await getEmailsByUserIds(editorIds)

  const content = `
    <p>
      The science officer has changed the approval status of article 
      ${formatManuscriptTitle(version.title)}.
    </p>
    ${getArticleLink(version.manuscriptId)}
  `

  const data = {
    content,
    subject: 'Approval status change',
    to: editorEmails,
  }

  sendEmail(data)
}

const mapper = {
  articleAccepted,
  articleRejected,
  articleRevision,
  chat,
  currentlyWith,
  dataTypeSelected,
  fullSubmission,
  identityVerification,
  initialSubmission,
  reinviteReviewer,
  reviewerInvitationResponse,
  reviewerInvited,
  reviewSubmitted,
  revisionSubmitted,
  scienceOfficerApprovalStatusChange,
}

const email = async (type, context) => {
  if (!mapper[type])
    throw new Error(`${type} is not a valid email notification type`)

  await mapper[type](context)
}

module.exports = email
