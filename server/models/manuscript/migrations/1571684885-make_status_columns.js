exports.up = knex =>
  knex.schema.table('manuscripts', table => {
    table
      .boolean('isInitiallySubmitted')
      .defaultTo(false)
      .notNullable()

    table
      .boolean('isDataTypeSelected')
      .defaultTo(false)
      .notNullable()
  })
