const omit = require('lodash/omit')

const PubsweetTeamMember = require('@pubsweet/model-team/src/team_member')

class TeamMember extends PubsweetTeamMember {
  static get schema() {
    return {
      type: 'object',
      required: ['teamId', 'userId'],
    }
  }

  static get relationMappings() {
    return omit(PubsweetTeamMember.relationMappings, 'alias')
  }
}

module.exports = TeamMember
