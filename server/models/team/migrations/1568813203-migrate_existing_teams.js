const { transaction } = require('objection')

const { Team, TeamMember } = require('@pubsweet/models')
const logger = require('@pubsweet/logger')

exports.up = async knex => {
  let teamEntities = []

  try {
    teamEntities = await knex('entities').where(
      knex.raw(`data ->> 'type' = 'team'`),
    )
  } catch (err) {
    logger.error(
      'Migrate existing teams: Failed to get team entities. Most likely "entities" table does not exist',
      err,
    )
  }

  await Promise.all(
    teamEntities.map(async team => {
      const { data } = team

      const teamData = {
        name: data.name,
        role: data.teamType,
      }

      if (data.global) teamData.global = true

      if (data.object) {
        if (!data.object.objectId || !data.object.objectType)
          throw new Error(
            'Migrate existing teams: Team entity has an object but the object has missing properties',
          )

        teamData.objectId = data.object.objectId
        teamData.objectType = data.object.objectType
      }

      try {
        await transaction(Team.knex(), async trx => {
          const query = { role: teamData.role }
          if (teamData.global) {
            query.global = true
          } else {
            query.objectId = teamData.objectId
          }

          const teamExists = await Team.query(trx).findOne(query)

          let dbTeam
          if (!teamExists) {
            dbTeam = await Team.query(trx).insert(teamData)
          } else {
            dbTeam = teamExists
          }

          const teamMemberData =
            data.members &&
            data.members.map(memberId => ({
              teamId: dbTeam.id,
              userId: memberId,
            }))

          await Promise.all(
            teamMemberData.map(async memberData => {
              const memberExists = await TeamMember.query(trx).findOne(
                memberData,
              )
              if (!memberExists) await TeamMember.query(trx).insert(memberData)
            }),
          )
        })
      } catch (e) {
        logger.error('Migrate existing teams: Failed! Rolling back...')
        throw new Error(e)
      }
    }),
  )
}
