/* eslint-disable sort-keys */

const path = require('path')
const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const CompressionPlugin = require('compression-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const fs = require('fs-extra')
const config = require('config')
const { pick } = require('lodash')

// TO DO -- maybe dotenv should be a proper dependency (not dev)
const env = require('dotenv').config({ path: 'config/development.env' })

const rules = require('./common-rules')

// const outputPath = path.resolve(__dirname, '..', '_build', 'assets')
const outputPath = path.resolve(__dirname, '..', '_build')

// can't use node-config in webpack so save whitelisted client config into the build and alias it below
const clientConfig = pick(config, config.publicKeys)
fs.ensureDirSync(outputPath)
const clientConfigPath = path.join(outputPath, 'client-config.json')
fs.writeJsonSync(clientConfigPath, clientConfig, { spaces: 2 })

const {
  WEBPACK_DEV_SERVER_PORT: devServerPort = 4000,
  SERVER_PORT: serverPort = 3000,
} = env

const serverUrl = `localhost:${serverPort}`
const serverUrlHttp = `http://${serverUrl}`
const serverUrlWs = `ws://${serverUrl}`

const devServer = {
  // compress: true,
  // contentBase: outputPath,
  historyApiFallback: true,
  hot: true,
  port: devServerPort,
  proxy: {
    '/subscriptions': {
      target: serverUrlWs,
      ws: true,
    },
    '/api': serverUrlHttp,
    '/graphql': serverUrlHttp,
    '/uploads': serverUrlHttp,
  },
  publicPath: '/',
  // publicPath: '/assets/',
}

module.exports = {
  devServer,
  name: 'app',
  target: 'web',
  mode: 'development',
  context: path.join(__dirname, '..', 'app'),
  entry: {
    app: [
      'react-hot-loader/patch',
      // 'webpack-hot-middleware/client',
      './app',
    ],
  },
  output: {
    path: outputPath,
    filename: '[name].js',
    publicPath: '/',
    // publicPath: '/assets/',
  },
  devtool: 'cheap-module-source-map',
  module: {
    rules,
  },
  resolve: {
    modules: [
      path.resolve(__dirname, '..'),
      path.resolve(__dirname, '..', 'node_modules'),
      'node_modules',
    ],
    alias: {
      joi: 'joi-browser',
      config: clientConfigPath,
    },
    extensions: ['.js', '.jsx', '.json', '.scss'],
    enforceExtension: false,
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"development"',
    }),
    new HtmlWebpackPlugin({
      title: 'microPublication',
      template: '../app/index.ejs', // Load a custom template
      inject: 'body', // Inject all scripts into the body
    }),
    // put dynamically required modules into the build
    new webpack.ContextReplacementPlugin(/./, __dirname, {
      [config.authsome.mode]: config.authsome.mode,
      [config.validations]: config.validations,
    }),
    new CopyWebpackPlugin([{ from: '../static' }]),
    new webpack.optimize.AggressiveMergingPlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new CompressionPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$/,
      threshold: 10240,
      minRatio: 0.8,
    }),
  ],
  node: {
    fs: 'empty',
    __dirname: true,
  },
}
