/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'
import { AbstractEditor } from '../../xpubEdit'

const stripHTML = html => {
  const tmp = document.createElement('DIV')
  tmp.innerHTML = html
  return tmp.textContent || tmp.innerText || ''
}

const isHTMLNotEmpty = html => stripHTML(html).length > 0

const Wrapper = styled.div`
  align-items: center;
  border-left: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  display: flex;
  margin-bottom: calc(${th('gridUnit')});

  > div:first-child {
    flex-grow: 1;
  }
`

const Title = styled(AbstractEditor)`
  border: 0;
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeHeading4')};
  line-height: ${th('lineHeightHeading4')};
  margin: 0 0 0 ${th('gridUnit')};
  padding: 0;
`

const SectionItem = props => {
  const { className, rightComponent, title } = props
  const titleValue = isHTMLNotEmpty(title) ? title : 'Untitled'

  return (
    <Wrapper className={className}>
      <Title
        onBlur={() => {}}
        onChange={() => {}}
        readonly
        value={titleValue || 'Untitled'}
      />
      {rightComponent}
    </Wrapper>
  )
}

export default SectionItem
