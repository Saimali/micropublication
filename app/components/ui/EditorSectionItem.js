/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'

import { Action, ActionGroup } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import AssignToManuscript from './AssignToManuscript'
import SectionItemWithStatus from './SectionItemWithStatus'

const Wrapper = styled.div`
  border-bottom: ${th('borderWidth')} ${th('borderStyle')}
    ${th('colorFurniture')};
  margin-bottom: ${th('gridUnit')};
`

const EditorToolRow = styled.div`
  display: flex;
  margin-bottom: ${th('gridUnit')};
`

const ActionsWrapper = styled.div`
  flex-shrink: 0;
`

const Separator = styled.span`
  width: calc(${th('gridUnit')} * 2);
`

const Actions = ({ articleId }) => (
  <ActionsWrapper>
    <ActionGroup>
      <Action
        data-test-id={`go-to-article-link-${articleId}`}
        to={`/article/${articleId}`}
      >
        Go to Article
      </Action>
    </ActionGroup>
  </ActionsWrapper>
)

const EditorSectionItem = props => {
  const {
    allEditors,
    allScienceOfficers,
    id: articleId,
    author,
    authorIds,
    editor,
    scienceOfficer,
    updateAssignedEditor,
    updateAssignedScienceOfficer,
    variant,
  } = props
  const ActionsComponent = <Actions articleId={articleId} />

  return (
    <Wrapper>
      <SectionItemWithStatus
        actionsComponent={ActionsComponent}
        author={author}
        {...props}
      />

      {variant === 'editor' && (
        <EditorToolRow>
          <AssignToManuscript
            allOptions={allEditors}
            articleId={articleId}
            authorIds={authorIds}
            currentlyAssigned={editor}
            label="Editor"
            update={updateAssignedEditor}
          />

          <Separator />

          <AssignToManuscript
            allOptions={allScienceOfficers}
            articleId={articleId}
            authorIds={authorIds}
            currentlyAssigned={scienceOfficer}
            label="Science Officer"
            update={updateAssignedScienceOfficer}
          />
        </EditorToolRow>
      )}
    </Wrapper>
  )
}

export default EditorSectionItem
