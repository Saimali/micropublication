import styled from 'styled-components'

import { Accordion as UIAccordion } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

const Accordion = styled(UIAccordion)`
  box-shadow: 0 1px ${th('colorPrimary')};
  height: calc(${th('gridUnit')} * 3);
  margin: ${th('gridUnit')} 0;
  transition: box-shadow 0.1s ease-in;

  &:hover {
    box-shadow: 0 2px ${th('colorPrimary')};
  }

  > span {
    font-variant-ligatures: none;
    font-weight: bold;
  }
`

export default Accordion
