/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { Tab } from '@pubsweet/ui'

const TabsContainer = styled.div`
  display: flex;
  margin-bottom: calc(${th('gridUnit')} * 2);

  > :last-child {
    > div {
      border-right: ${th('borderWidth')} ${th('borderStyle')}
        ${th('colorPrimary')};
    }
  }
`

const Title = styled.div`
  border-bottom: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  padding: ${th('gridUnit')} 1em;
`

const TabContainer = styled.div.attrs(props => ({
  'data-test-id': props['data-test-id'] || 'tab-container',
}))``

const Content = styled.div``

class Tabs extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      activeKey: props.activeKey || null,
    }
  }

  componentDidMount() {
    const { activeKey } = this.props
    // eslint-disable-next-line react/no-did-mount-set-state
    this.setState({ activeKey })
  }

  setActiveKey(activeKey) {
    this.setState({ activeKey })
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(activeKey)
    }
  }

  render() {
    const {
      activeKey: propsActiveKey,
      alwaysUseActiveKeyFromProps,
      onTabClick,
      sections,
      title,
    } = this.props
    const { activeKey: stateActiveKey } = this.state

    const activeKey = alwaysUseActiveKeyFromProps
      ? propsActiveKey
      : stateActiveKey

    return (
      <React.Fragment>
        <TabsContainer>
          {title && <Title>{title}</Title>}

          {sections.map(({ key, label }) => (
            <TabContainer key={key} onClick={() => this.setActiveKey(key)}>
              <Tab active={activeKey === key} onClick={() => onTabClick(key)}>
                {label || key}
              </Tab>
            </TabContainer>
          ))}
        </TabsContainer>

        {activeKey && (
          <Content>
            {sections.find(section => section.key === activeKey).content}
          </Content>
        )}
      </React.Fragment>
    )
  }
}

export default Tabs
