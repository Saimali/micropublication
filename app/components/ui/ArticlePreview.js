/* eslint-disable react/prop-types */
import React from 'react'
import styled from 'styled-components'
import { Toggle } from 'react-powerplug'

import { Button } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import Manuscript from '../../../ui/src/preview/Manuscript'

import DiscreetButton from './DiscreetButton'
import ChatModal from './ChatModal'
import PageHeader from './PageHeader'
import { withCurrentUser } from '../../userContext'

const Wrapper = styled.div`
  font-family: ${th('fontReading')};
  margin: 0 auto;
  max-width: 1024px;
  padding: 0 calc(${th('gridUnit')} * 7) 0 calc(${th('gridUnit')} * 6);
`

const HeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`

const ExportWrapper = styled.div`
  display: flex;
  margin-left: auto;

  button {
    align-self: center;
  }
`

const ChatButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`

const Preview = props => {
  const {
    article,
    // livePreview,
    previousVersion,
    showAdditionalData,
    // showDiff,
  } = props

  return (
    <Manuscript
      previousVersion={previousVersion}
      showAdditionalData={showAdditionalData}
      version={article}
    />
  )
}

const ArticlePreview = props => {
  // TO DO -- article is the version and article id is the manuscript id. very confusing
  const {
    article,
    articleId,
    authorChatMessages,
    currentUser,
    exportManuscript,
    livePreview,
    previousVersion,
    sendAuthorChatMessage,
    showHeader = true,
  } = props

  const { decision } = article
  const accepted = decision === 'accept'
  const { isGlobal } = currentUser.auth
  const isAuthor = currentUser.auth.isAuthor.includes(articleId)

  const showDiff = true
  const showAdditionalData = isGlobal || isAuthor

  return (
    <Wrapper>
      {!livePreview && !accepted && showHeader && (
        <>
          <PageHeader>Article Preview</PageHeader>

          <Toggle initial={false}>
            {({ on, toggle }) => (
              <ChatButtonWrapper>
                {isAuthor && (
                  <DiscreetButton onClick={toggle}>
                    chat with the editors
                  </DiscreetButton>
                )}

                <ChatModal
                  headerText="Chat with the editors"
                  isOpen={on}
                  messages={authorChatMessages}
                  onRequestClose={toggle}
                  sendMessage={sendAuthorChatMessage}
                />
              </ChatButtonWrapper>
            )}
          </Toggle>
        </>
      )}

      {!livePreview && accepted && (
        <HeaderWrapper>
          {showHeader && <PageHeader>Article Preview</PageHeader>}

          {!isAuthor && isGlobal && (
            <ExportWrapper>
              <Button onClick={() => exportManuscript(articleId)} primary>
                Export to HTML
              </Button>
            </ExportWrapper>
          )}
        </HeaderWrapper>
      )}

      {article && (
        <Preview
          article={article}
          livePreview={livePreview}
          previousVersion={previousVersion}
          showAdditionalData={showAdditionalData}
          showDiff={showDiff}
        />
      )}
      {!article && 'No data'}
    </Wrapper>
  )
}

export default withCurrentUser(ArticlePreview)
