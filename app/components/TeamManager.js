/* eslint-disable react/prop-types */

import React from 'react'
import { Query, Mutation } from '@apollo/react-components'
import styled from 'styled-components'
import { Form, Formik } from 'formik'
import { keys, sortBy } from 'lodash'
import gql from 'graphql-tag'

import { Button, H4 } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import { PageHeader as DefaultPageHeader, Select } from './ui'
import { UPDATE_TEAM } from './compose/pieces/updateTeam'
import { GET_TEAMS } from './compose/pieces/getTeams'
import { GET_USERS } from './compose/pieces/getUsers'

import Loading from './Loading'

const CLEAN_UP_GLOBAL_TEAM_MEMBERSHIP = gql`
  mutation CleanUpGlobalTeamMembership {
    cleanUpGlobalTeamMembership
  }
`

const PageHeader = styled(DefaultPageHeader)`
  margin-block-end: ${th('gridUnit')};
`

const teamNameMapper = {
  editors: 'Editors',
  scienceOfficers: 'Science Officers',
}

const TeamHeadingWrapper = styled(H4)`
  font-size: ${th('fontSizeHeading4')};
  line-height: ${th('lineHeightHeading3')};
  margin: 0 auto ${th('gridUnit')};
`

const TeamSectionWrapper = styled.div`
  padding-bottom: calc(${th('gridUnit')} * 2);
`

const Ribbon = styled.div`
  background: ${th('colorSuccess')};
  border-radius: 3px;
  color: ${th('colorTextReverse')};
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  padding: calc(${th('gridUnit')} / 2);
  text-align: center;
  visibility: ${props => (props.hide ? 'hidden' : 'visible')};
`

const ButtonWrapper = styled.div`
  padding: calc(${th('gridUnit')} * 2) 0;
`

const PageWrapper = styled.div`
  margin: 0 auto;
  max-width: 800px;
`

const TeamHeading = props => {
  const { name } = props
  return <TeamHeadingWrapper>{name}</TeamHeadingWrapper>
}

const TeamSection = props => {
  const { setFieldValue, type, users, value } = props
  const name = teamNameMapper[type]

  const options = users
    ? users.map(user => ({
        label: user.displayName,
        value: user.id,
      }))
    : []

  const selectValue = value.map(user => {
    if (!user.label && !user.value)
      return {
        label: user.user.displayName, // FIX ME -- streamline how team members are structured on the server
        value: user.user.id,
      }
    return user
  })

  const handleChange = newValue => {
    setFieldValue(type, newValue)
  }

  return (
    <TeamSectionWrapper>
      <TeamHeading name={name} />
      <Select
        closeMenuOnSelect={false}
        isMulti
        name={type}
        onChange={handleChange}
        options={options}
        value={selectValue}
      />
    </TeamSectionWrapper>
  )
}

const TeamManagerForm = props => {
  const { setFieldValue, teams, users, values } = props

  return (
    <Form>
      {teams.map(team => (
        <TeamSection
          key={team.id}
          setFieldValue={setFieldValue}
          type={team.role}
          users={users}
          value={values[team.role]}
        />
      ))}

      <ButtonWrapper>
        <Button disabled={!props.dirty} primary type="submit">
          Save
        </Button>
      </ButtonWrapper>
    </Form>
  )
}

class TeamManager extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      hideRibbon: true,
    }
  }

  handleSubmit = (formValues, formikBag) => {
    const { cleanUp, updateTeam, teams } = this.props

    const data = keys(formValues).map(role => ({
      id: teams.find(t => t.role === role).id,
      members: formValues[role].map(item => {
        // FIX ME -- standardize this mess outside of the ui
        if (item.user && item.user.id) return { user: { id: item.user.id } }
        if (item.id) return { user: { id: item.id } }
        return { user: { id: item.value } }
      }),
    }))

    const promises = data.map(team =>
      updateTeam({
        variables: {
          id: team.id,
          input: { members: team.members },
        },
      }),
    )

    Promise.all(promises).then(res => {
      this.showRibbon()
      formikBag.resetForm(formValues)
      cleanUp()
    })
  }

  // TODO -- handle better cases like many quick saves
  showRibbon = () => {
    this.setState({
      hideRibbon: false,
    })

    setTimeout(
      () =>
        this.setState({
          hideRibbon: true,
        }),
      4000,
    )
  }

  render() {
    const { users, loading, teams } = this.props
    const { hideRibbon } = this.state

    if (loading) return <Loading />

    let globalTeams = teams.filter(team => team.global)
    const infoMessage = 'Your teams have been successfully updated'

    const initialValues = {}
    globalTeams.forEach(team => {
      initialValues[team.role] = team.members
    })

    globalTeams = sortBy(globalTeams, 'name')

    return (
      <PageWrapper>
        <PageHeader>Team Manager</PageHeader>
        <Ribbon hide={hideRibbon}>{infoMessage}</Ribbon>

        <Formik
          initialValues={initialValues}
          onSubmit={this.handleSubmit}
          render={formikProps => (
            <TeamManagerForm
              teams={globalTeams}
              users={users}
              {...formikProps}
            />
          )}
        />
      </PageWrapper>
    )
  }
}

export default () => (
  <Mutation mutation={UPDATE_TEAM} refetchQueries={[{ query: GET_TEAMS }]}>
    {updateTeam => (
      <Mutation mutation={CLEAN_UP_GLOBAL_TEAM_MEMBERSHIP}>
        {cleanUp => (
          <Query query={GET_TEAMS}>
            {({ data: { teams }, loading }) => (
              <Query query={GET_USERS}>
                {({ data: { users } }) => (
                  <TeamManager
                    cleanUp={cleanUp}
                    loading={loading}
                    teams={teams}
                    updateTeam={updateTeam}
                    users={users}
                  />
                )}
              </Query>
            )}
          </Query>
        )}
      </Mutation>
    )}
  </Mutation>
)
