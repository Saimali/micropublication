/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { get, isEmpty } from 'lodash'
import { Toggle } from 'react-powerplug'

import { th } from '@pubsweet/ui-toolkit'
import { Button } from '@pubsweet/ui'

import { ArticlePreviewModal } from './ui'
import { SubmissionConfirmationModal } from '../ui'
import Dropdown from './formElements/Dropdown'
import InitialSubmission from './formElements/InitialSubmission'
import GeneExpressionForm from './formElements/GeneExpressionForm'

const InfoError = styled.div`
  color: ${th('colorError')};
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  margin-top: ${th('gridUnit')};
`

const options = {
  dataType: [
    {
      label: 'Gene expression results',
      value: 'geneExpression',
    },
    {
      label: 'No Datatype',
      value: 'noDatatype',
    },
  ],
}

const DatatypeSelect = props => {
  const { isGlobal, values } = props

  const disabledSelect = (
    <Dropdown
      data-test-id="datatype-select"
      error={get(props.errors, 'dataType')}
      isDisabled
      label="Choose a datatype"
      name="dataType"
      options={options.dataType}
      required
      touched={get(props.touched, 'dataType')}
      value={options.dataType.find(o => o.value === get(values, 'dataType'))}
      {...props}
    />
  )

  if (!isGlobal) return disabledSelect

  return (
    <Dropdown
      data-test-id="datatype-select"
      error={get(props.errors, 'dataType')}
      label="Choose a datatype"
      name="dataType"
      options={options.dataType}
      required
      touched={get(props.touched, 'dataType')}
      value={options.dataType.find(o => o.value === get(values, 'dataType'))}
      {...props}
    />
  )
}

const SubmissionForm = props => {
  const {
    datatypeSelected,
    errors,
    full,
    handleSubmit,
    initial,
    isAuthor,
    isGlobal,
    isValid,
    readOnly,
    revisionState,
    submitCount,
    values,
  } = props

  const initialSubmissionState = !initial
  const dataTypeSelectionState = initial && !datatypeSelected
  const fullSubmissionState = initial && datatypeSelected && !full

  return (
    <React.Fragment>
      <InitialSubmission readOnly={readOnly} values={values} {...props} />

      {dataTypeSelectionState && (
        <DatatypeSelect isGlobal={isGlobal} values={values} {...props} />
      )}

      {(fullSubmissionState || revisionState) &&
        values.dataType === 'geneExpression' &&
        isAuthor && <GeneExpressionForm readOnly={readOnly} {...props} />}

      <Toggle initial={false}>
        {({ on, toggle }) => (
          <React.Fragment>
            <Button onClick={toggle}>Preview</Button>
            <ArticlePreviewModal
              isOpen={on}
              onRequestClose={toggle}
              values={values}
            />
          </React.Fragment>
        )}
      </Toggle>

      {(initialSubmissionState || fullSubmissionState || revisionState) &&
        isValid && (
          <Toggle intial={false}>
            {({ on, toggle }) => (
              <React.Fragment>
                <Button onClick={toggle} primary>
                  Submit
                </Button>

                <SubmissionConfirmationModal
                  isFullSubmission={fullSubmissionState && !revisionState}
                  isInitialSubmission={initialSubmissionState}
                  isOpen={on}
                  isRevisionSubmission={revisionState}
                  onConfirm={handleSubmit}
                  onRequestClose={toggle}
                />
              </React.Fragment>
            )}
          </Toggle>
        )}

      {(dataTypeSelectionState || !isValid) && (
        <Button data-test-id="submit-button" primary type="submit">
          Submit
        </Button>
      )}

      {submitCount > 0 && !isEmpty(errors) && !isValid && (
        <InfoError>
          Please review submission and fill out all required fields.
        </InfoError>
      )}
    </React.Fragment>
  )
}

export default SubmissionForm
