/* eslint-disable react/prop-types */

import React, { Fragment } from 'react'
import styled from 'styled-components'
import { first, last } from 'lodash'

import { DateParser } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import ComposedEditorPanel from './compose/EditorPanel'
import Loading from './Loading'
import { shouldShowAssignReviewersLink } from '../helpers/status'
import TabContext from '../tabContext'

import {
  DecisionSection,
  DiscussSection,
  EditorPanelMetadata,
  EditorPanelRibbon,
  PageHeader,
  PanelInfo,
  ReviewerInfo,
  ScienceOfficerSection,
  Tabs,
} from './ui'

const Wrapper = styled.div`
  padding-right: calc(${th('gridUnit')} * 2);
`

const StyledPageHeader = styled(PageHeader)`
  margin-bottom: 0;
  margin-top: calc(${th('gridUnit')} * 2);
`

const Decision = ({ approved, version, submitDecision }) => (
  <DecisionSection
    approvedByScienceOfficer={approved}
    data={version}
    submitDecision={submitDecision}
  />
)

const Tab = props => {
  const {
    articleId,
    authorChatMessages,
    currentUser,
    doi,
    editor,
    isAdmin,
    isEditor,
    isLastSubmittedVersion,
    isScienceOfficer,
    reinviteReviewer,
    // requestReviewerAttention,
    reviewerChatThreads,
    scienceOfficer,
    scienceOfficerChatMessages,
    sendAuthorChatMessage,
    sendReviewerChatMessage,
    sendSOChatMessage,
    setSOApproval,
    showReviewerAssignmentLink,
    submitDecision,
    updateMetadata,
    version,
  } = props

  const {
    authors,
    decision,
    editorSuggestedReviewers,
    isApprovedByScienceOfficer,
    latest,
    previousReviewers,
    reviewerCounts,
    reviews,
  } = version

  const author = authors.find(a => a.submittingAuthor)
  const alreadyRejected = decision === 'reject'
  const approved = isApprovedByScienceOfficer === true
  const notApproved = isApprovedByScienceOfficer === false
  const scienceOfficerHasDecision = approved || notApproved

  const deriveRibbonStatus = () => {
    if (decision === 'accept') return 'accepted'
    if (decision === 'reject') return 'rejected'
    if (decision === 'revise') return 'revise'
    if (approved) return 'scienceOfficerApproved'
    if (notApproved) return 'scienceOfficerDeclined'
    if (!scienceOfficerHasDecision) return 'scienceOfficerPending'
    return null
  }

  return (
    <Fragment>
      <EditorPanelRibbon type={deriveRibbonStatus()} />

      {latest && (
        <PanelInfo
          author={author}
          authorChatMessages={authorChatMessages}
          editor={editor}
          isScienceOfficer={isScienceOfficer}
          scienceOfficer={scienceOfficer}
          sendAuthorChatMessage={sendAuthorChatMessage}
        />
      )}

      {!alreadyRejected && !decision && (isScienceOfficer || isAdmin) && (
        <ScienceOfficerSection
          approved={isApprovedByScienceOfficer}
          editorSuggestedReviewers={editorSuggestedReviewers}
          setSOApproval={setSOApproval}
        />
      )}

      {latest && (
        <DiscussSection
          currentUser={currentUser}
          data={scienceOfficerChatMessages}
          sendChat={sendSOChatMessage}
        />
      )}

      <ReviewerInfo
        articleId={articleId}
        previousReviewers={previousReviewers}
        reinviteReviewer={reinviteReviewer}
        // requestReviewerAttention={requestReviewerAttention}
        reviewerChatThreads={reviewerChatThreads}
        reviewerCounts={reviewerCounts}
        reviews={reviews}
        sendReviewerChatMessage={sendReviewerChatMessage}
        showReviewerAssignmentLink={
          showReviewerAssignmentLink && isLastSubmittedVersion
        }
      />

      {(decision || (!decision && (isEditor || isAdmin))) && (
        <Decision
          approved={isApprovedByScienceOfficer}
          submitDecision={submitDecision}
          version={version}
        />
      )}

      {latest && (
        <EditorPanelMetadata
          articleId={articleId}
          doi={doi}
          updateMetadata={updateMetadata}
        />
      )}
    </Fragment>
  )
}

const EditorPanel = props => {
  const {
    article,
    currentUser,
    editor,
    loading,
    reinviteReviewer,
    // requestReviewerAttention,
    reviewerChatThreads,
    scienceOfficer,
    sendAuthorChatMessage,
    sendReviewerChatMessage,
    sendSOChatMessage,
    setSOApproval,
    submitDecision,
    updateMetadata,
    versions,
  } = props

  if (loading) return <Loading />

  const isAdmin = currentUser.admin
  const isEditor = currentUser.auth.isGlobalEditor
  const isScienceOfficer = currentUser.auth.isGlobalScienceOfficer

  const { id: articleId, chatThreads, doi } = article
  const soChat = chatThreads.find(
    thread => thread.chatType === 'scienceOfficer',
  )
  const soChatMessages = soChat.messages

  const authorChat = chatThreads.find(thread => thread.chatType === 'author')
  const authorChatMessages = authorChat.messages

  const lastSubmittedVersion = last(versions.filter(v => v.submitted))
  const firstSubmittedVersion = first(versions.filter(v => v.submitted))
  const showReviewerAssignmentLink = shouldShowAssignReviewersLink(article)

  const sections = versions.map((version, index) => ({
    content: (
      <Tab
        articleId={articleId}
        authorChatMessages={authorChatMessages}
        currentUser={currentUser}
        doi={doi}
        editor={editor}
        isAdmin={isAdmin}
        isEditor={isEditor}
        isLastSubmittedVersion={version.id === lastSubmittedVersion.id}
        isScienceOfficer={isScienceOfficer}
        reinviteReviewer={reinviteReviewer}
        // requestReviewerAttention={requestReviewerAttention}
        reviewerChatThreads={reviewerChatThreads}
        scienceOfficer={scienceOfficer}
        scienceOfficerChatMessages={soChatMessages}
        sendAuthorChatMessage={sendAuthorChatMessage}
        sendReviewerChatMessage={sendReviewerChatMessage}
        sendSOChatMessage={sendSOChatMessage}
        setSOApproval={setSOApproval}
        showReviewerAssignmentLink={showReviewerAssignmentLink}
        submitDecision={submitDecision}
        updateMetadata={updateMetadata}
        version={version}
      />
    ),
    key: version.id,
    label: (
      <DateParser
        dateFormat="MM.DD.YY HH:mm"
        timestamp={new Date(Number(version.created))}
      >
        {timestamp => (
          <span>
            {firstSubmittedVersion.id === version.id
              ? `Original: ${timestamp}`
              : `Revision ${index}: ${timestamp}`}
          </span>
        )}
      </DateParser>
    ),
  }))

  return (
    <TabContext.Consumer>
      {({ activeTab, locked, updateActiveTab }) => (
        <Wrapper>
          <StyledPageHeader>Editor Panel</StyledPageHeader>
          <Tabs
            activeKey={activeTab || last(versions).id}
            alwaysUseActiveKeyFromProps={locked}
            onTabClick={updateActiveTab}
            sections={sections}
          />
        </Wrapper>
      )}
    </TabContext.Consumer>
  )
}

const Composed = () => <ComposedEditorPanel render={EditorPanel} />
export default Composed
