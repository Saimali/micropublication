/* eslint-disable react/prop-types */

import React from 'react'
import { Mutation } from '@apollo/react-components'
import gql from 'graphql-tag'
import { GET_ARTICLE } from './getArticle'

const SUBMIT_MANUSCRIPT = gql`
  mutation SubmitManuscript($input: SubmissionFormInput!) {
    submitManuscript(input: $input)
  }
`

const SubmitManuscriptMutation = props => {
  const { articleId, render } = props

  const refetchQueries = [
    {
      query: GET_ARTICLE,
      variables: { id: articleId },
    },
  ]

  return (
    <Mutation mutation={SUBMIT_MANUSCRIPT} refetchQueries={refetchQueries}>
      {(submitManuscript, submitManuscriptResponse) =>
        render({ submitManuscript, submitManuscriptResponse })
      }
    </Mutation>
  )
}

export default SubmitManuscriptMutation
