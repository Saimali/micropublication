/* eslint-disable react/prop-types */

import React from 'react'
import { Mutation } from '@apollo/react-components'
import gql from 'graphql-tag'

import { GET_ARTICLE_REVIEWERS } from './getArticleReviewers'

const INVITE_REVIEWER = gql`
  mutation InviteReviewer(
    $manuscriptVersionId: ID!
    $input: InviteReviewerInput!
  ) {
    inviteReviewer(manuscriptVersionId: $manuscriptVersionId, input: $input) {
      id
    }
  }
`

const InviteReviewerMutation = props => {
  const { articleId, render } = props

  const refetchQueries = [
    {
      query: GET_ARTICLE_REVIEWERS,
      variables: { id: articleId },
    },
  ]

  return (
    <Mutation mutation={INVITE_REVIEWER} refetchQueries={refetchQueries}>
      {(inviteReviewer, inviteReviewerResponse) =>
        render({ inviteReviewer, inviteReviewerResponse })
      }
    </Mutation>
  )
}

export default InviteReviewerMutation
