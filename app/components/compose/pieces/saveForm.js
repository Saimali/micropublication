/* eslint-disable react/prop-types */
import React from 'react'
import { Mutation } from '@apollo/react-components'
import gql from 'graphql-tag'

import { GET_ARTICLE } from './getArticle'
import { DASHBOARD_MANUSCRIPTS } from './dashboardManuscripts'
import { withCurrentUser } from '../../../userContext'

const SAVE_FORM = gql`
  mutation saveSubmissionForm($input: SubmissionFormInput!) {
    saveSubmissionForm(input: $input)
  }
`

const SaveFormMutation = props => {
  const { articleId, currentUser, render } = props

  const refetchQueries = [
    {
      query: GET_ARTICLE,
      variables: { id: articleId },
    },
    {
      query: DASHBOARD_MANUSCRIPTS,
      variables: { reviewerId: currentUser.id },
    },
  ]

  return (
    <Mutation mutation={SAVE_FORM} refetchQueries={refetchQueries}>
      {(saveForm, saveFromResponse) => render({ saveForm, saveFromResponse })}
    </Mutation>
  )
}

export default withCurrentUser(SaveFormMutation)
