/* eslint-disable react/prop-types */

import React from 'react'
import { Query } from '@apollo/react-components'
import gql from 'graphql-tag'

const GET_ARTICLE_REVIEWERS = gql`
  query GetArticleReviewers($id: ID!) {
    manuscript(id: $id) {
      id
      versions {
        id
        submitted
        suggestedReviewer {
          name
          WBId
        }
        teams {
          id
          role
          members {
            id
            status
            user {
              id
              agreedTc
              displayName
            }
          }
        }
      }
    }
  }
`

const GetArticleReviewersQuery = props => {
  const { articleId: id, render } = props

  return (
    <Query query={GET_ARTICLE_REVIEWERS} variables={{ id }}>
      {render}
    </Query>
  )
}

export { GET_ARTICLE_REVIEWERS }
export default GetArticleReviewersQuery
