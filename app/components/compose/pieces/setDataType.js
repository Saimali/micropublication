/* eslint-disable react/prop-types */

import React from 'react'
import { Mutation } from '@apollo/react-components'
import gql from 'graphql-tag'

import { GET_ARTICLE } from './getArticle'

const SET_DATA_TYPE = gql`
  mutation SetDataType($manuscriptId: ID!, $input: SetDataTypeInput!) {
    setDataType(manuscriptId: $manuscriptId, input: $input)
  }
`

const SetDataTypeMutation = props => {
  const { articleId, render } = props

  const refetchQueries = [
    {
      query: GET_ARTICLE,
      variables: { id: articleId },
    },
  ]

  return (
    <Mutation mutation={SET_DATA_TYPE} refetchQueries={refetchQueries}>
      {(setDataType, setDataTypeResponse) =>
        render({ setDataType, setDataTypeResponse })
      }
    </Mutation>
  )
}

export default SetDataTypeMutation
