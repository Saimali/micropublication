/* eslint-disable react/prop-types */

import React, { useState } from 'react'
import styled from 'styled-components'
import Dropzone from 'react-dropzone'
import { get } from 'lodash'

import { th } from '@pubsweet/ui-toolkit'

// TO DO -- extract Labels from TextField
const Label = styled.label`
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
`

const StyledDropzone = styled(Dropzone)`
  border: 1px dashed ${th('colorFurniture')};
  border-radius: 5px;
  display: flex;
  height: calc(${th('gridUnit')} * 12);
  margin-top: ${th('gridUnit')};
  width: 400px;

  p {
    align-self: center;
    color: ${th('colorTextPlaceholder')};
    flex-grow: 1;
    text-align: center;
  }
`

const Img = styled.img`
  max-width: 600px;
`

class DropArea extends React.Component {
  constructor(props) {
    super(props)

    const file = {
      name: props.values.image.name,
      preview: `/uploads/${props.values.image.url}`,
      url: props.values.image.url,
    }

    this.state = {
      file,
      uploading: false,
    }

    this.maxSize = 10 * 1024 * 1024
  }

  handleDrop = fileList => {
    const file = fileList[0]
    if (!file) return
    const { name, setFieldValue, setInternalError, upload } = this.props

    this.setState({
      file,
      uploading: true,
    })

    upload(file)
      .then(res => {
        this.setState({
          uploading: false,
        })

        setFieldValue(name, {
          name: file.name,
          url: res.data.upload.url,
        })
      })
      .catch(e => {
        this.setState({
          uploading: false,
        })

        setInternalError('An error occured during upload')
        setTimeout(() => {
          setInternalError(null)
        }, 5000)
      })
  }

  handleDropRejected = rejectedFiles => {
    const file = rejectedFiles[0]
    const { name, setFieldTouched, setInternalError } = this.props

    let error = 'Something went wrong, please re-upload the file'

    if (file.size > this.maxSize) {
      error = 'Image size size must be no greater than 10 MB'
    }

    if (!['image/jpeg', 'image/png'].includes(file.type))
      error = 'Image type must be either jpeg or png'

    setInternalError(error)
    setFieldTouched(name)

    setTimeout(() => {
      setInternalError(null)
    }, 5000)
  }

  render() {
    const { readOnly } = this.props
    const { file, uploading } = this.state

    return (
      <div data-test-id={this.props['data-test-id']}>
        {uploading && <div>Uploading...</div>}
        {!readOnly && (
          <StyledDropzone
            accept="image/jpeg, image/png"
            data-test-id={`${this.props['data-test-id']}-dropzone`}
            maxSize={this.maxSize}
            onDrop={this.handleDrop}
            onDropRejected={this.handleDropRejected}
            style={{}}
          >
            <p>
              Drop an image (jpeg or png under 10 MB) <br />
              here {file && 'to replace current'} or click to select
            </p>
          </StyledDropzone>
        )}
        {file && <Img alt="" src={file.preview} />}
        {file && (
          <div>
            {/* <a href={file.url}> */}
            {file.name}
            {/* </a> */}
          </div>
        )}
      </div>
    )
  }
}

const StyledDropArea = styled(DropArea)`
  div > div:first-of-type {
    border-style: solid;
    height: 200px;
    width: 400px;
  }
`

const Error = styled.span`
  color: ${th('colorError')};
  font-size: ${th('fontSizeBaseSmall')};
  padding-left: ${th('gridUnit')};
`

const Wrapper = styled.div`
  margin-bottom: calc(${th('gridUnit')} * 2);
`

const Image = props => {
  const [internalError, setInternalError] = useState(null)
  const formError = get(props.errors, 'image.url')
  const touched = get(props.touched, 'image')

  const error = internalError || (touched && formError)

  return (
    <Wrapper>
      {props.label && (
        <>
          <Label>{`${props.label}${props.required && ' *'}`}</Label>
          {error && <Error>{error}</Error>}
        </>
      )}
      <StyledDropArea {...props} setInternalError={setInternalError} />
    </Wrapper>
  )
}

export default Image
