import { css } from 'styled-components'
import { merge } from 'lodash'

import cokoTheme from '@pubsweet/coko-theme'
import { th } from '@pubsweet/ui-toolkit'

const activeTab = css`
  background: ${th('colorPrimary')};
  color: ${th('colorTextReverse')};

  :hover {
    background: ${th('colorPrimary')};
  }
`

const Tab = css`
  border-color: ${th('colorPrimary')};
  border-right: 0;
  border-top: 0;
  border-width: ${th('borderWidth')};
  color: ${th('colorText')};
  cursor: pointer;
  transition: all 0.1s ease-in;
  user-select: none;

  /* stylelint-disable-next-line order/properties-alphabetical-order, order/order */
  ${props => props.active && activeTab};
`

const AccordionRoot = css`
  cursor: default;
`

const Button = css`
  ${cokoTheme.cssOverrides.ui.Button};

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  cursor: pointer;
  line-height: calc(${th('gridUnit')} * 3);
`

const edits = {
  colorAddition: 'palegreen',
  colorRemoval: 'lightcoral',
  cssOverrides: {
    ui: {
      Accordion: {
        Root: AccordionRoot,
      },
      Button,
      Tab,
    },
  },
}

const editedTheme = merge(cokoTheme, edits)
export default editedTheme
