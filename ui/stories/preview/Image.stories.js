import React from 'react'

import Image from '../../src/preview/Image'

import ImageFile from './_sampleFiles/sample.png'
import PreviousImageFile from './_sampleFiles/sample2.png'

export const Base = () => <Image name="sample_name" source={ImageFile} />

export const WithDiff = () => (
  <Image
    name="sample_name"
    previousSource={PreviousImageFile}
    source={ImageFile}
  />
)

WithDiff.story = {
  name: 'Providing a previous image will trigger diff',
}

export const HideRemoved = () => (
  <Image
    name="sample_name"
    previousSource={PreviousImageFile}
    showRemoved={false}
    source={ImageFile}
  />
)

HideRemoved.story = {
  name: 'Show Diff, Hide Removed',
}

export const HideDiff = () => (
  <Image
    name="sample_name"
    previousSource={PreviousImageFile}
    showDiff={false}
    source={ImageFile}
  />
)

HideDiff.story = {
  name: 'Diff triggered, but hidden completely',
}

export default {
  component: Image,
  title: 'Preview/Image',
}
