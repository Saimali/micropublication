import React from 'react'
import { lorem } from 'faker'

import TextBlock from '../../src/preview/TextBlock'

export const Base = () => <TextBlock value={lorem.sentences(6)} />

export const WithLabel = () => (
  <TextBlock label={lorem.words(3)} value={lorem.sentences(6)} />
)

const label = lorem.words(3)
const previousValue = lorem.sentences(8)
const newValue = `${previousValue
  .split('. ')
  .slice(0, 2)
  .join('. ')}. ${previousValue
  .split('. ')
  .slice(3)
  .join('. ')} ${lorem.sentence()}`

export const WithDiff = () => (
  <TextBlock label={label} previousValue={previousValue} value={newValue} />
)

export const WithDiffHideRemoved = () => (
  <TextBlock
    label={label}
    previousValue={previousValue}
    showRemoved={false}
    value={newValue}
  />
)

export const WithDiffHideAll = () => (
  <TextBlock
    label={label}
    previousValue={previousValue}
    showDiff={false}
    value={newValue}
  />
)

export const WholeTextWasAdded = () => (
  <TextBlock isAdded value={lorem.sentences(8)} />
)

export const WholeTextWasRemoved = () => (
  <TextBlock isRemoved value={lorem.sentences(8)} />
)

export default {
  component: TextBlock,
  title: 'Preview/Text Block',
}
