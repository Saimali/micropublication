import React from 'react'
import { lorem } from 'faker'
import { cloneDeep } from 'lodash'

import ObserveExpression from '../../src/preview/ObserveExpression'

const originalData = {
  certainly: [
    {
      certainly: {
        name: lorem.words(2),
      },
      during: {
        name: '',
      },
      subcellularLocalization: {
        name: '',
      },
    },
    {
      certainly: {
        name: '',
      },
      during: {
        name: '',
      },
      subcellularLocalization: {
        name: lorem.words(2),
      },
    },
  ],
  not: [
    {
      during: {
        name: '',
      },
      not: {
        name: lorem.words(2),
      },
      subcellularLocalization: {
        name: '',
      },
    },
  ],
  partially: [
    {
      during: {
        name: '',
      },
      partially: {
        name: '',
      },
      subcellularLocalization: {
        name: lorem.words(2),
      },
    },
  ],
  possibly: [
    {
      during: {
        name: lorem.words(2),
      },
      possibly: {
        name: '',
      },
      subcellularLocalization: {
        name: '',
      },
    },
  ],
}

const newData = cloneDeep(originalData)
newData.partially.push({
  during: {
    name: '',
  },
  partially: {
    name: lorem.words(2),
  },
  subcellularLocalization: {
    name: '',
  },
})

newData.certainly = originalData.certainly.slice(0, 1)

export const Base = () => <ObserveExpression data={originalData} />

export const WithDiff = () => (
  <ObserveExpression data={newData} previousData={originalData} />
)

export const HideRemoved = () => (
  <ObserveExpression
    data={newData}
    previousData={originalData}
    showRemoved={false}
  />
)

export const HideDiffs = () => (
  <ObserveExpression
    data={newData}
    previousData={originalData}
    showDiff={false}
  />
)

export default {
  component: ObserveExpression,
  title: 'Preview/Observe Expression',
}
