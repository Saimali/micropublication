import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { get } from 'lodash'

import { th } from '@pubsweet/ui-toolkit'

import Checkbox from '../common/Checkbox'

import AdditionalData from './AdditionalData'
import Affiliation from './Affiliation'
import Author from './Author'
import CorrespondingAuthors from './CorrespondingAuthors'
import DiffArray from './DiffArray'
import Editor from './Editor'
import EditorList from './EditorList'
import Image from './Image'
import Separator from './Separator'
import TextBlock from './TextBlock'

import { transformAuthorData } from './_helpers'

const Wrapper = styled.div`
  margin-bottom: calc(${th('gridUnit')} * 4);
  text-align: justify;

  > div:not(:last-child) {
    margin-bottom: ${th('gridUnit')};
  }
`

const Manuscript = props => {
  const {
    showAdditionalData,
    showDiff: showDiffFromProps,
    version,
    previousVersion,
  } = props

  const {
    acknowledgements,
    authors,
    comments,
    dataType,
    funding,
    geneExpression,
    laboratory,
    image,
    imageCaption,
    methods,
    patternDescription,
    reagents,
    references,
    suggestedReviewer,
    title,
  } = version

  const diffOn = showDiffFromProps && !!previousVersion

  const [showDiff, setShowDiff] = useState(false)
  const [showRemoved, setShowRemoved] = useState(true)

  const {
    affiliationData,
    authorData,
    correspondingAuthorData,
  } = transformAuthorData(authors)

  const {
    affiliationData: previousAffiliationData,
    authorData: previousAuthorData,
    correspondingAuthorData: previousCorrespondingAuthorData,
  } = transformAuthorData(get(previousVersion, 'authors'))

  // console.log(methods, previousVersion.methods)

  const imageSource =
    image &&
    image.url &&
    (image.url.match(/^blob/) ? image.url : `/uploads/${image.url}`)

  return (
    <Wrapper>
      {diffOn && (
        <>
          <Checkbox
            checkBoxText="Show changes"
            checked={showDiff}
            onChange={() => setShowDiff(!showDiff)}
          />

          {showDiff && (
            <Checkbox
              checkBoxText="Show removed"
              checked={showRemoved}
              onChange={() => setShowRemoved(!showRemoved)}
            />
          )}

          <Separator />
        </>
      )}

      <Editor
        previousValue={get(previousVersion, 'title')}
        showDiff={showDiff}
        showRemoved={showRemoved}
        value={title}
        variant="title"
      />

      <div>
        <DiffArray
          component={Author}
          currentArray={authorData}
          previousArray={previousAuthorData}
          showDiff={showDiff}
          showRemoved={showRemoved}
        />
      </div>

      <div>
        <DiffArray
          component={Affiliation}
          currentArray={affiliationData}
          previousArray={previousAffiliationData}
          showDiff={showDiff}
          showRemoved={showRemoved}
        />
      </div>

      <CorrespondingAuthors
        data={correspondingAuthorData}
        previousData={previousCorrespondingAuthorData}
        showDiff={showDiff}
        showRemoved={showRemoved}
      />

      <Image
        name={image.name}
        previousSource={get(previousVersion, 'image.source')}
        showDiff={showDiff}
        showRemoved={showRemoved}
        source={imageSource}
      />

      <Editor
        previousValue={get(previousVersion, 'imageCaption')}
        showDiff={showDiff}
        showRemoved={showRemoved}
        value={imageCaption}
        variant="caption"
      />

      <Editor
        label="description"
        previousValue={get(previousVersion, 'patternDescription')}
        showDiff={showDiff}
        showRemoved={showRemoved}
        value={patternDescription}
      />

      <Editor
        label="methods"
        previousValue={get(previousVersion, 'methods')}
        showDiff={showDiff}
        showRemoved={showRemoved}
        value={methods}
      />

      <Editor
        label="reagents"
        previousValue={get(previousVersion, 'reagents')}
        showDiff={showDiff}
        showRemoved={showRemoved}
        value={reagents}
      />

      <EditorList
        label="references"
        previousValues={
          get(previousVersion, 'references') &&
          previousVersion.references.map(ref => ref.reference)
        }
        showDiff={showDiff}
        showRemoved={showRemoved}
        values={references && references.map(ref => ref.reference)}
      />

      <TextBlock
        label="acknowledgements"
        previousValue={get(previousVersion, 'acknowledgements')}
        showDiff={showDiff}
        showRemoved={showRemoved}
        value={acknowledgements}
      />

      <TextBlock
        label="funding"
        previousValue={get(previousVersion, 'funding')}
        showDiff={showDiff}
        showRemoved={showRemoved}
        value={funding}
      />

      {showAdditionalData && (
        <AdditionalData
          comments={comments}
          dataType={dataType}
          geneExpression={geneExpression}
          laboratory={get(laboratory, 'name')}
          previousComments={get(previousVersion, 'comments')}
          previousDataType={get(previousVersion, 'dataType')}
          previousGeneExpression={get(previousVersion, 'geneExpression')}
          previousLaboratory={get(previousVersion, 'laboratory.name')}
          previousSuggestedReviewer={get(
            previousVersion,
            'suggestedReviewer.name',
          )}
          showDiff={showDiff}
          showRemoved={showRemoved}
          suggestedReviewer={get(suggestedReviewer, 'name')}
        />
      )}
    </Wrapper>
  )
}

Manuscript.propTypes = {
  /** Control showing of "additional data" section */
  showAdditionalData: PropTypes.bool,
  /** You can hardcode showing / not showing diffs with this */
  showDiff: PropTypes.bool,
  /** Manuscript version */
  version: PropTypes.shape({
    acknowledgements: PropTypes.string,
    authors: PropTypes.arrayOf(
      PropTypes.shape({
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        correspondingAuthor: PropTypes.bool,
        email: PropTypes.string,
      }),
    ),
    comments: PropTypes.string,
    dataType: PropTypes.string,
    funding: PropTypes.string,
    geneExpression: PropTypes.shape({
      antibodyUsed: PropTypes.string,
      backboneVector: PropTypes.shape({
        name: PropTypes.string,
      }),
      coinjected: PropTypes.string,
      constructComments: PropTypes.string,
      detectionMethod: PropTypes.string,
      dnaSequence: PropTypes.arrayOf(
        PropTypes.shape({
          name: PropTypes.string,
        }),
      ),
      expressionPattern: PropTypes.shape({
        name: PropTypes.string,
      }),
      fusionType: PropTypes.shape({
        name: PropTypes.string,
      }),
      genotype: PropTypes.string,
      injectionConcentration: PropTypes.string,
      inSituDetails: PropTypes.string,
      integratedBy: PropTypes.shape({
        name: PropTypes.string,
      }),
      reporter: PropTypes.shape({
        name: PropTypes.string,
      }),
      species: PropTypes.shape({
        name: PropTypes.string,
      }),
      strain: PropTypes.string,
      transgeneName: PropTypes.string,
      transgeneUsed: PropTypes.arrayOf(
        PropTypes.shape({
          name: PropTypes.string,
        }),
      ),
      utr: PropTypes.shape({
        name: PropTypes.string,
      }),
      variation: PropTypes.shape({
        name: PropTypes.string,
      }),
      observeExpression: PropTypes.shape({
        certainly: PropTypes.arrayOf(
          PropTypes.shape({
            certainly: PropTypes.shape({
              name: PropTypes.string,
            }),
            during: PropTypes.shape({
              name: PropTypes.string,
            }),
            subcellularLocalization: PropTypes.shape({
              name: PropTypes.string,
            }),
          }),
        ),
        possibly: PropTypes.arrayOf(
          PropTypes.shape({
            possibly: PropTypes.shape({
              name: PropTypes.string,
            }),
            during: PropTypes.shape({
              name: PropTypes.string,
            }),
            subcellularLocalization: PropTypes.shape({
              name: PropTypes.string,
            }),
          }),
        ),
        probably: PropTypes.arrayOf(
          PropTypes.shape({
            probably: PropTypes.shape({
              name: PropTypes.string,
            }),
            during: PropTypes.shape({
              name: PropTypes.string,
            }),
            subcellularLocalization: PropTypes.shape({
              name: PropTypes.string,
            }),
          }),
        ),
        not: PropTypes.arrayOf(
          PropTypes.shape({
            not: PropTypes.shape({
              name: PropTypes.string,
            }),
            during: PropTypes.shape({
              name: PropTypes.string,
            }),
            subcellularLocalization: PropTypes.shape({
              name: PropTypes.string,
            }),
          }),
        ),
      }),
    }),
    image: PropTypes.shape({
      name: PropTypes.string,
      url: PropTypes.string,
    }),
    imageCaption: PropTypes.string,
    laboratory: PropTypes.shape({
      name: PropTypes.string,
    }),
    methods: PropTypes.string,
    patternDescription: PropTypes.string,
    reagents: PropTypes.string,
    references: PropTypes.arrayOf(
      PropTypes.shape({
        reference: PropTypes.string,
      }),
    ),
    suggestedReviewer: PropTypes.shape({
      name: PropTypes.string,
    }),
    title: PropTypes.string,
  }).isRequired,
  /** Same as version. For diffs. If existent and showDiff is true (it is by default), diffs will be triggered. */
  previousVersion: PropTypes.shape({
    acknowledgements: PropTypes.string,
    authors: PropTypes.arrayOf(
      PropTypes.shape({
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        correspondingAuthor: PropTypes.bool,
        email: PropTypes.string,
      }),
    ),
    comments: PropTypes.string,
    dataType: PropTypes.string,
    funding: PropTypes.string,
    geneExpression: PropTypes.shape({
      antibodyUsed: PropTypes.string,
      backboneVector: PropTypes.shape({
        name: PropTypes.string,
      }),
      coinjected: PropTypes.string,
      constructComments: PropTypes.string,
      detectionMethod: PropTypes.string,
      dnaSequence: PropTypes.arrayOf(
        PropTypes.shape({
          name: PropTypes.string,
        }),
      ),
      expressionPattern: PropTypes.shape({
        name: PropTypes.string,
      }),
      fusionType: PropTypes.shape({
        name: PropTypes.string,
      }),
      genotype: PropTypes.string,
      injectionConcentration: PropTypes.string,
      inSituDetails: PropTypes.string,
      integratedBy: PropTypes.shape({
        name: PropTypes.string,
      }),
      reporter: PropTypes.shape({
        name: PropTypes.string,
      }),
      species: PropTypes.shape({
        name: PropTypes.string,
      }),
      strain: PropTypes.string,
      transgeneName: PropTypes.string,
      transgeneUsed: PropTypes.arrayOf(
        PropTypes.shape({
          name: PropTypes.string,
        }),
      ),
      utr: PropTypes.shape({
        name: PropTypes.string,
      }),
      variation: PropTypes.shape({
        name: PropTypes.string,
      }),
      observeExpression: PropTypes.shape({
        certainly: PropTypes.arrayOf(
          PropTypes.shape({
            certainly: PropTypes.shape({
              name: PropTypes.string,
            }),
            during: PropTypes.shape({
              name: PropTypes.string,
            }),
            subcellularLocalization: PropTypes.shape({
              name: PropTypes.string,
            }),
          }),
        ),
        possibly: PropTypes.arrayOf(
          PropTypes.shape({
            possibly: PropTypes.shape({
              name: PropTypes.string,
            }),
            during: PropTypes.shape({
              name: PropTypes.string,
            }),
            subcellularLocalization: PropTypes.shape({
              name: PropTypes.string,
            }),
          }),
        ),
        probably: PropTypes.arrayOf(
          PropTypes.shape({
            probably: PropTypes.shape({
              name: PropTypes.string,
            }),
            during: PropTypes.shape({
              name: PropTypes.string,
            }),
            subcellularLocalization: PropTypes.shape({
              name: PropTypes.string,
            }),
          }),
        ),
        not: PropTypes.arrayOf(
          PropTypes.shape({
            not: PropTypes.shape({
              name: PropTypes.string,
            }),
            during: PropTypes.shape({
              name: PropTypes.string,
            }),
            subcellularLocalization: PropTypes.shape({
              name: PropTypes.string,
            }),
          }),
        ),
      }),
    }),
    image: PropTypes.shape({
      name: PropTypes.string,
      url: PropTypes.string,
    }),
    imageCaption: PropTypes.string,
    laboratory: PropTypes.shape({
      name: PropTypes.string,
    }),
    methods: PropTypes.string,
    patternDescription: PropTypes.string,
    reagents: PropTypes.string,
    references: PropTypes.arrayOf(
      PropTypes.shape({
        reference: PropTypes.string,
      }),
    ),
    suggestedReviewer: PropTypes.shape({
      name: PropTypes.string,
    }),
    title: PropTypes.string,
  }),
}

Manuscript.defaultProps = {
  previousVersion: null,
  showAdditionalData: false,
  showDiff: true,
}

export default Manuscript
