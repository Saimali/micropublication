const stripHTML = html => {
  const tmp = document.createElement('DIV')
  tmp.innerHTML = html
  return tmp.textContent || tmp.innerText || ''
}

const isHTMLEmpty = html => stripHTML(html).length === 0

/* eslint-disable-next-line import/prefer-default-export */
export { isHTMLEmpty, stripHTML }
