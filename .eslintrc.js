module.exports = {
  parser: 'babel-eslint',
  env: {
    es6: true,
    browser: true,
  },
  extends: 'pubsweet',
  rules: {
    'sort-keys': 'off',
    'import/no-extraneous-dependencies': [
      'error',
      {
        // devDependencies: ['client/.storybook/**', 'client/stories/**'],
        devDependencies: [
          '.storybook/**',
          'ui/stories/**',
          'webpack/webpack.development.config.js',
        ],
      },
    ],
  },
}
